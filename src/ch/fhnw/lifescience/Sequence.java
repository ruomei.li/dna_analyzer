package ch.fhnw.lifescience;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

public class Sequence {

    private static Logger LOG = LogManager.getLogger(Main.class);

    public static final String A = "A";
    public static final String C = "C";
    public static final String G = "G";
    public static final String T = "T";

    private String data;

    public Sequence(String seq) {
        this.data = seq;
    }

    public String getRawData() {
        return data;
    }

    /**
     * Returns the amount of a given nucleotide.
     * @param nucleotide
     * @return as described
     */
    public int count(String nucleotide) {
        int result = 0;

        // YOUR CODE
         Character refCh = nucleotide.charAt(0);
         for (Character ch: data.toCharArray()) {
             if (ch.equals(refCh)) {
                 result++;
             }
         }
        return result;
    }

    /**
     * Returns a map with the count of all nucleotide.
     * @return as described
     */
    public Map<String, Integer> countAll() {
        Map<String, Integer> result = new HashMap<>();

        // YOUR CODE
        result.put(Sequence.A,count(Sequence.A));
        result.put(Sequence.C,count(Sequence.C));
        result.put(Sequence.T,count(Sequence.T));
        result.put(Sequence.G,count(Sequence.G));

        return result;
    }
}



