package ch.fhnw.lifescience;

import java.util.Map;


public class Main {

    public static void main(String [] args)
    {

        if (args.length != 1)
        {
            System.out.println("Wrong number of arguments");
            return;
        }
        try
        {
            Sequence seq = FastaReader.read(args[0]);

            Map<String, Integer> nucleotideMap = seq.countAll();

            for (String k: nucleotideMap.keySet())
            {
                System.out.println(k + ": " + nucleotideMap.get(k));
            }
        }
        catch (Exception ex) // change
                //change 2
        {
            ex.printStackTrace();
        }
    }
}
