package ch.fhnw.lifescience;

import java.io.*;

public class FastaReader {

    /**
     * This method reads a fasta file into a string object.
     * @param filename
     * @return String object containing the sequence
     * @throws Exception In case of an error
     */
    public static Sequence read(String filename) throws Exception {
        StringBuffer result = new StringBuffer();
        BufferedReader reader = new BufferedReader(new FileReader(filename));
        String line = reader.readLine();
        while (line != null) {
            if (line.startsWith(">")) {
                String info = line.substring(1);
            } else {
                result.append(line);
            }
            line = reader.readLine();
        }

        return new Sequence(result.toString());
    }
}
