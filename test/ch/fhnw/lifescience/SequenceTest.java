package ch.fhnw.lifescience;

import org.junit.Assert;
import org.junit.Test;

import java.util.Map;

public class SequenceTest {

    @Test
    public void testCount() {
        String s = "ACGTACGTACGT";
        Sequence seq = new Sequence(s);
        Assert.assertEquals(3, seq.count("A"));
    }

    @Test
    public void testCountAll() {
        String s = "ACGTACGTACGT";
        Sequence seq = new Sequence(s);
        Map<String, Integer> result = seq.countAll();
        Assert.assertEquals(3, (long) result.get("A"));
        Assert.assertEquals(3, (long) result.get("G"));
        Assert.assertEquals(3, (long) result.get("T"));
        Assert.assertEquals(3, (long) result.get("G"));
    }
}
